#ifndef ZENG_H
#define ZENG_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class zeng; }
QT_END_NAMESPACE

class zeng : public QMainWindow
{
    Q_OBJECT

public:
    zeng(QWidget *parent = nullptr);
    ~zeng();

private:
    Ui::zeng *ui;
};
#endif // ZENG_H
